﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="AutofacAsmxIoc.Web._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Hello Unity!
    </h2>
    <p>
		Returned message from the web service: 
    	<asp:Label ID="lblMessage" Font-Bold="true" ForeColor="Blue" runat="server" Text="Label"></asp:Label>
    </p>
</asp:Content>
