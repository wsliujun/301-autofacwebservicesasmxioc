﻿using System;
using AutofacAsmxIoc.Web.UnityServices;

namespace AutofacAsmxIoc.Web
{
	public partial class _Default : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			string message = null;

			using(var service = new DummyService())
			{
				message = service.HelloWorld(" wsliujun1@163.com");
			}

			lblMessage.Text = message;
		}
	}
}
