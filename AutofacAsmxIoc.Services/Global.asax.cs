﻿using System;
using AutofacContrib.CommonServiceLocator;
using Microsoft.Practices.ServiceLocation;

namespace AutofacAsmxIoc.Services
{
    public class Global : System.Web.HttpApplication 
	{
		protected void Application_Start(object sender, EventArgs e)
		{
            var container = ContainerProvider.CreateContainer();
            ServiceLocator.SetLocatorProvider(() => new AutofacServiceLocator(container));
		}
	}
}