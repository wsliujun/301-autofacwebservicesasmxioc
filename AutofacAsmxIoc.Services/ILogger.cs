﻿
namespace AutofacAsmxIoc.Services
{
	public interface ILogger
	{
		void Write(string message);
	}
}
