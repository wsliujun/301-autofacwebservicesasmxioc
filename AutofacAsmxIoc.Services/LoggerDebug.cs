﻿using System.Diagnostics;

namespace AutofacAsmxIoc.Services
{
	public class LoggerDebug : ILogger
	{
		public void Write(string message)
		{
			Debug.WriteLine(message);
		}
	}
}
