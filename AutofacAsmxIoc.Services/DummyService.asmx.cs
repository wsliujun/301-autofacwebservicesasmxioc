﻿using System.Web.Services;
using Microsoft.Practices.ServiceLocation;

namespace AutofacAsmxIoc.Services
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class DummyService : WebService
    {

        [WebMethod]
        public string HelloWorld(string name)
        {
            string message = string.Format("Hello World, {0}!", name);

            var handler = ServiceLocator.Current.GetInstance<ILogger>();
            handler.Write(message);

            return message;
        }
    }
}