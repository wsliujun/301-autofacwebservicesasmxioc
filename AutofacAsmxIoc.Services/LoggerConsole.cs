﻿using System;

namespace AutofacAsmxIoc.Services
{
	public class LoggerConsole : ILogger
	{
		public void Write(string message)
		{
			Console.WriteLine(message);
		}
	}
}
