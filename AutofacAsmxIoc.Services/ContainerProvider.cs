﻿using Autofac;

namespace AutofacAsmxIoc.Services
{
    public class ContainerProvider
    {
        public ContainerProvider()
        {
        }

        private static IContainer _container;
        private static IContainer Container
        {
            get { return _container; }
            set { _container = value; }
        }

        public static IContainer CreateContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<LoggerDebug>().AsImplementedInterfaces();
            //builder.RegisterType<ConsoleLogger>().AsImplementedInterfaces();
            Container = builder.Build();
            return Container;
        }
    }
}